%global debug_package %{nil}
%bcond_with test

Name:           perl-Test-File
Version:        1.99.4
Release:        1
Summary:        A Perl module which provides test functions to check file attributes and data
License:        Artistic License 2.0 or GPL+
URL:            https://metacpan.org/release/Test-File
Source0:        https://cpan.metacpan.org/authors/id/B/BR/BRIANDFOY/Test-File-1.994.tar.gz

BuildArch:      noarch
BuildRequires:  make perl
%if %{with test}
BuildRequires:  perl-Test-utf8
%endif

Provides:       perl(Test::File) = 1.993

%description
The perl-Test-File package contains the Test::File perl module, which
provides convenience test functions to check file attributes and data
in a Test::More fashion.

%prep
%autosetup -n Test-File-1.994 -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1 NO_PERLLOCAL=1
%make_build

%install
%make_install
%{_fixperms} -c %{buildroot}

%check
%if %{with test}
make test
%endif

%files
%doc README.pod Changes
%license LICENSE
%{perl_vendorlib}/Test/
%{_mandir}/man3/Test::File.3*

%changelog
* Tue Jan 14 2025 sqfu <dev01203@linx-info.com> - 1.99.4-1
- update version to 1.99.4
- refresh distro and move to BRIANDFOY

* Thu Mar 28 2024 Xu Jin <jinxu@kylinos.cn> - 1.99.3-1
- Upgrade to version 1.99.3

* Tue Jun 14 2022 SimpleUpdate Robot <tc@openeuler.org> - 1.99.2-1
- Upgrade to version 1.99.2

* Wed Jun 16 2021 wulei <wulei80@huawei.com> - 1.44.3-9
- fixes failed: perl command not found

* Mon Aug 17 2020 lingsheng <lingsheng@huawei.com> - 1.44.3-8
- Fix build fail

* Mon Jan 06 2020 Jiangping Hu <hujp1985@foxmail.com> - 1.44.3-7
- Package init
